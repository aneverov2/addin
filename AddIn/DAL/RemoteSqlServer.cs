﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using AddIn.DAL.Models;

namespace AddIn.DAL
{
    public class RemoteSqlServer : DbContext
    {
        public RemoteSqlServer()
            : base("name=LocalSqlServer")
        {
        }
        //public RemoteSqlServer(DC<RemoteSqlServer> options)
        //    : base(options)
        //{
        //}
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }

        public virtual DbSet<OcProduct> oc_product { get; set; }
    }
}
