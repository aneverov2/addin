﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AddIn.DAL.Models;

namespace AddIn.DAL.Repositories
{
    interface IProductRepository : IRepository<OcProduct>
    {
    }
}
