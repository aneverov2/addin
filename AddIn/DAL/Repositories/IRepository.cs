﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddIn.DAL.Models;

namespace AddIn.DAL.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        void AddAsync(T entity);
        IEnumerable<T> GetAll();
        T GetById(int id);
        //void Update(T entity);
        void Delete(T entity);
        void ExplicitLoading(Func<T, bool> func);
    }
}
