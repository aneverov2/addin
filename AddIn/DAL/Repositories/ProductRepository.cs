﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AddIn.DAL.Models;

namespace AddIn.DAL.Repositories
{
    public class ProductRepository : Repository<OcProduct>, IProductRepository
    {
        public ProductRepository(RemoteSqlServer context) : base(context)
        {
        }
    }
}
