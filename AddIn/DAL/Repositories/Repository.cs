﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddIn.DAL.Models;

namespace AddIn.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected RemoteSqlServer Context;
        protected DbSet<T> DbSet { get; set; }

        public Repository(RemoteSqlServer context)
        {
            Context = context;
            DbSet = Context.Set<T>();
        }

        public void AddAsync(T entity)
        {
            var entry = DbSet.Add(entity);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }

        public virtual T GetById(int id)
        {
            return DbSet.FirstOrDefault(e => e.Id == id);
        }

        //public void Update(T entity)
        //{
        //    DbSet.Update(entity);
        //}

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
        }

        public void ExplicitLoading(Func<T, bool> func)
        {
            DbSet.Where(func).AsQueryable().Load();
        }
    }
}
