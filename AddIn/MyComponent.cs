﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using AddIn.DAL.Models;
using AddIn.DAL.Repositories;
using MySql.Data.MySqlClient;

/// <summary>Интерфейс с объявлениями пользовательских методов и свойств компоненты</summary>
/// 
[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
public interface IMyComponent
{
    /// <summary>Пример реализации свойства компоненты</summary>
    string Prop { get; set; }

    /// <summary>Пример реализации метода компоненты</summary>
    int Go(int Param1, int Param2);

    int GetData(int Param1, int Param2);
    int Test(int Param1, int Param2);
    void UploadProduct(string code, string product, string articul, string comment);

}

namespace AddIn
{
    /// <summary>Класс, реализующий пользовательские методы компоненты</summary>
    [ProgId("AddIn.MyComponent")]
    [ClassInterface(ClassInterfaceType.None)]
    public class MyComponent : AddIn, IMyComponent
    {
        /// <summary>Хранилище для значения свойства Prop</summary>
        private string prop;
        private IRepository<OcProduct> _db;
        public MyComponent(IRepository<OcProduct> db)
        {
            _db = db;
        }


        /// <summary>Реализация свойства Prop</summary>
        public string Prop
        {
            get
            {
                return prop;
            }
            set
            {
                prop = value;
            }

        }

        /// <summary>Функция для проверки доступности компоненты</summary>
        public int Go(int Param1, int Param2)
        {
            try
            {
                return (Param1 / Param2);
            }
            catch (Exception e)
            {
                asyncEvent.ExternalEvent("AddIn", "error", e.ToString());
                return 0;
            }
        }

        public MySqlConnection GetDbConnection()
        {
            // Connection String.
            String connString = "Server=" + host + ";Database=" + database
                                + ";port=" + port + ";User Id=" + username + ";password=" + password;

            MySqlConnection conn = new MySqlConnection(connString);

            return conn;
        }

        public int GetData(int Param1, int Param2)
        {
            MySqlConnection conn = GetDbConnection();
            try
            {
                Console.WriteLine("Openning Connection ...");
                DataTable dt = new DataTable();
                conn.Open();
                string sql = "SELECT * FROM `oc_product`"; // Строка запроса
                MySqlCommand com = new MySqlCommand(sql, conn);
                using (MySqlDataReader dr = com.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        dt.Load(dr);
                    }
                }

                int result = dt.Columns.Count;
                return result;

                //System.Windows.Forms.MessageBox.Show("Сообщение")
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                return 0;
            }

        }

        public int Test(int Param1, int Param2)
        {
            List<OcProduct> list = _db.GetAll().ToList();
            return list.Count;
        }

        public void UploadProduct(string code, string product, string articul, string comment)
        {
            //MySqlConnection conn = GetDbConnection();
            //conn.Open();
            //string sql = "SELECT * FROM `oc_product`"; // Строка запроса
            //MySqlCommand com = new MySqlCommand(sql, conn);
            //using (MySqlDataAdapter dr = com.ExecuteReader())
            //{
            //    if (dr.HasRows)
            //    {
            //        dt.Load(dr);
            //    }
            //}

        }
    }
}

